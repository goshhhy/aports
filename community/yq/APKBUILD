# Contributor: Tuan Hoang <tmhoang@linux.ibm.com>
# Maintainer: Tuan Hoang <tmhoang@linux.ibm.com>
pkgname=yq
pkgver=4.12.0
pkgrel=0
pkgdesc="portable command-line YAML processor written in Go"
url="https://github.com/mikefarah/yq"
# riscv64 blocked by govendor
arch="all !riscv64"
license="MIT"
makedepends="go govendor"
options="chmod-clean"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikefarah/yq/archive/v$pkgver.tar.gz"

build() {
	GOPATH="$srcdir" go build -v
}

check() {
	GOPATH="$srcdir" go test
}

package() {
	install -Dm755 yq "$pkgdir"/usr/bin/yq
	for file in LICENSE README.md; do
		install -Dm644 $file "$pkgdir"/usr/share/doc/$pkgname/$file
	done

	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
			"$pkgdir"/usr/share/zsh/site-functions \
			"$pkgdir"/usr/share/fish/completions
	"$pkgdir"/usr/bin/yq shell-completion bash \
		> "$pkgdir"/usr/share/bash-completion/completions/yq.bash
	"$pkgdir"/usr/bin/yq shell-completion zsh \
		> "$pkgdir"/usr/share/zsh/site-functions/_yq
	"$pkgdir"/usr/bin/yq shell-completion fish \
		> "$pkgdir"/usr/share/fish/completions/yq.fish
}

sha512sums="
0e764fd45eaef9ff4bf8b1550657ae0b6120dd2ed8acd048c99f4f5c1c4ec165f19412ab34bfd21483605601734b87ea75c3f2cd17dc74be28159a113e7ba644  yq-4.12.0.tar.gz
"
