# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Sodface <sod@sodface.com>
pkgname=yt-dlp
pkgver=2021.08.10
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://github.com/yt-dlp/yt-dlp"
arch="noarch"
license="Unlicense"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-flake8 py3-nose py3-pytest"
subpackages="$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/yt-dlp/yt-dlp/archive/refs/tags/$pkgver.tar.gz"

build() {
	python3 setup.py build

	make completions
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Install completions
	install -Dm644 completions/bash/yt-dlp \
		-t "$pkgdir"/usr/share/bash-completion/completions
	install -Dm644 completions/zsh/_yt-dlp \
		-t "$pkgdir"/usr/share/zsh/site-functions
	install -Dm644 completions/fish/yt-dlp.fish \
		-t "$pkgdir"/usr/share/fish/completions
}

sha512sums="
2b2fa2b1424cf95dc611bec8354e0506239e684e42d4fdb921b2627a5c442cf2533d5446cedd3b8be7c884ceb1df9ab21dc32fffda4377559c253dbf04c614a4  yt-dlp-2021.08.10.tar.gz
"
