# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libphonenumber
pkgver=8.12.30
pkgrel=0
pkgdesc="Library for parsing, formatting, and validating international phone numbers."
url="https://github.com/googlei18n/libphonenumber"
arch="all"
license="Apache-2.0"
depends_dev="
	boost-dev
	icu-dev
	protobuf-dev
	"
makedepends="$depends_dev
	cmake
	gtest-dev
	ninja
	"
checkdepends="gtest"
subpackages="$pkgname-static $pkgname-dev"
source="https://github.com/googlei18n/libphonenumber/archive/v$pkgver/libphonenumber-v$pkgver.tar.gz
	cmake-duplicate-rule-definition.patch
	"

build() {
	cd cpp
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DREGENERATE_METADATA=OFF
	cmake --build build
}

check() {
	cd cpp
	./build/libphonenumber_test
}

package() {
	cd cpp
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8e62c4f53fe32bb4a8cacd213f9eea13b45ccc95598bf9e53a579bfd97dd4936d6ba5a919977adee17dd01be4dc3623bdc301354cc14b188a794b6baf3b90dcf  libphonenumber-v8.12.30.tar.gz
dc76f0649c401ec97a7449373b96247135c3b80cf1e5bb8afba005fed1055a74429d3c778b7519b609bdcfbb278be395ef83e0bd00228239b6f15d96b0d2df11  cmake-duplicate-rule-definition.patch
"
