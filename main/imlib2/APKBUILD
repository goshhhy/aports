# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=imlib2
pkgver=1.7.3
pkgrel=0
pkgdesc="Image manipulation library"
url="https://sourceforge.net/projects/enlightenment"
arch="all"
license="Imlib2"
subpackages="$pkgname-dev"
depends_dev="freetype-dev libxext-dev libsm-dev"
makedepends="$depends_dev tiff-dev giflib-dev bzip2-dev libpng-dev
	libid3tag-dev libjpeg-turbo-dev zlib-dev util-linux-dev libwebp-dev"
source="https://downloads.sourceforge.net/enlightenment/imlib2-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc/imlib2 \
		--x-libraries=/usr/lib \
		--disable-mmx \
		--disable-amd64 \
		--enable-visibility-hiding \
		--with-x \
		--with-bzip2 \
		--with-gif \
		--with-id3 \
		--with-jpeg \
		--with-png \
		--with-tiff \
		--with-webp \
		--with-zlib
	make
}

check() {
	make check
}

package() {
	make DESTDIR=$pkgdir install
}

sha512sums="
6accb10539eae878711742a52496875c1df66926a5f49f3d36b0eefbd73e21821c1bebc6ad50c099374dcf0959227924ad64de5a15f90c4105b11d029f4dc67a  imlib2-1.7.3.tar.gz
"
