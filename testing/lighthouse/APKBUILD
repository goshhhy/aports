# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=lighthouse
pkgver=1.5.1
pkgrel=0
pkgdesc="Ethereum 2.0 Client"
url="https://lighthouse.sigmaprime.io/"
arch="x86_64 aarch64"  # limited by upstream
license="Apache-2.0"
makedepends="cargo cmake openssl-dev protoc"
options="!check" # disable check as it takes too long
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/sigp/lighthouse/archive/v$pkgver/lighthouse-$pkgver.tar.gz"

export OPENSSL_NO_VENDOR=true
export RUSTFLAGS="$RUSTFLAGS -L /usr/lib/"

build() {
	cargo build --release --locked
}

check() {
	cargo test --release --locked \
		--workspace \
			--exclude ef_tests \
			--exclude eth1 \
			--exclude genesis
}

package() {
	install -D -m755 "target/release/lighthouse" "$pkgdir/usr/bin/lighthouse"

	install -Dm 644 -t "$pkgdir/usr/share/doc/lighthouse" README.md
}

sha512sums="
1a9bb021e9319c7e80919675c624b478d9488ba89b0fbb8f920d2874220e192f67b9d70f005ae1f005fd34cdf311a2a6ef4a7d82f799822f535c465a9c263df6  lighthouse-1.5.1.tar.gz
"
